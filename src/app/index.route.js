export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
  .state('root',{
    url:'/',
    abstract: true,
    templateUrl: 'app/layouts/baselayout.html'  	
  })
    .state('root.home', {
      url: '',
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'main'
    })
    .state('root.invoices', {
      url: 'invoice',
      abstract:true,
      template: '<ui-view></ui-view>'
    })
    .state('root.invoices.new', {
      url: '/new',
      templateUrl: 'app/invoices/new.html',
      controller: 'InvoiceController',
      controllerAs: 'invoice'
    });

  $urlRouterProvider.otherwise('/');
}
