/* global later:false */

import { InvoiceController } from './invoice.controller';
import { InvoiceScheduleController } from './invoice-schedule.controller';
import { InvoiceScheduleService } from './invoice-schedule.service';
import later from 'later';

let parse = later.parse;

angular.module('invoices',[])
  .constant('later',later)
  .constant('parse',parse)
  .controller('InvoiceController',InvoiceController)
  .controller('InvoiceScheduleController',InvoiceScheduleController)
  .service('InvoiceScheduleService',InvoiceScheduleService)
  .value('invoices_frequencies',[
	{
		name: 'weekly',
		label: 'weekly',
		schedule: parse.recur().every().weekOfMonth()
	},
	{
		name: 'every_second_week',
		label: 'every second week',
		schedule: parse.recur().every(2).weekOfMonth()
	},
	{
		name: 'every_third_week',
		label: 'every third week',
		schedule: parse.recur().every(3).weekOfMonth()
	},
	{
		name: 'monthly',
		label: 'monthly',
		schedule: parse.recur().every().month()
	},
	{
		name: 'every_second_month',
		label: 'every second month',
		schedule: parse.recur().every(2).month()
	},
	{
		name: 'quarterly',
		label: 'quarterly',
		schedule: parse.recur().every(3).month()
	},
	{
		name: 'every_half_year',
		label: 'every half year',
		schedule: parse.recur().every(6).month()
	},
	{
		name: 'yearly',
		label: 'yearly',
		schedule: parse.recur().every().year()
	}
]);