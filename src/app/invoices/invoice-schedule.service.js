export class InvoiceScheduleService{
	constructor(invoices_frequencies, later){
		'ngInject';
		this.later = later;

		this.frequencies = invoices_frequencies;
	}

	getSchedule(frequency, number,start, end){
		return this.later.schedule(frequency).next(number,start,end);
	}
}