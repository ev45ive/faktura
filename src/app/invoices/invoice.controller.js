export class InvoiceController{
	constructor(){
		'ngInject';

		this.items = [
			{
				name:'Test product',
				amount: 1,
				unit: 'pcs',
				net_price: 100,
				tax: 20,
				gross_price: 120,
				net_value: 100,
				gross_value: 120
			},
			{
				name:'Another product',
				amount: 2,
				unit: 'pcs',
				net_price: 100,
				tax: 20,
				gross_price: 120,
				net_value: 200,
				gross_value: 240
			}
		];
		this.net_total = 300;
		this.tax_total = 60;
		this.gross_total = 360;

		this.reccur = true;
	}
}