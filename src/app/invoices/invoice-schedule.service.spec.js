describe('Invoice schedule', function(){
  let frequencies, InvoiceSchedule;

  beforeEach(angular.mock.module('invoices'))

  beforeEach(inject((invoices_frequencies, later, InvoiceScheduleService) => {
    frequencies = invoices_frequencies;
    InvoiceSchedule = InvoiceScheduleService;
  }))

  it('should provide list of frequencies based on the configuration', () => {
    expect(frequencies.length).toBeGreaterThan(0)
  })

  it('should calculate number of invoice dates for given schedule, start and end dates', () => {

    let dates = [
     'Sun Jun 26 2016 02:00:00 GMT+0200 (CEST)',
     'Fri Jul 01 2016 02:00:00 GMT+0200 (CEST)',
     'Sun Jul 03 2016 02:00:00 GMT+0200 (CEST)',
     'Sun Jul 10 2016 02:00:00 GMT+0200 (CEST)', 
     'Sun Jul 17 2016 02:00:00 GMT+0200 (CEST)'
    ].map((date)=> new Date(date))

    const weekly = ((_)=>_.name==='weekly');

    let frequency = InvoiceSchedule.frequencies.filter(weekly).pop(),
      start = dates[0],
      end = dates[4],
      number = 5;

    let schedule = InvoiceSchedule.getSchedule(frequency.schedule, number, start, end);

    expect(schedule).toEqual(jasmine.any(Array));
    schedule.forEach((_,i)=>{
    	expect(_).toEqual(dates[i]);
    })
  })

})