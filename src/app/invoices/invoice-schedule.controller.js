export class InvoiceScheduleController{
	constructor(InvoiceScheduleService, $filter){
		'ngInject';

		this.dateFilter = $filter('date')

		this.service = InvoiceScheduleService;
		this.frequencies = InvoiceScheduleService.frequencies;

		this.default = {
			start: new Date(), // should be invouce date
			end: undefined, // something reasonable, like 1 year
			frequency: this.frequencies[0],
			description: ''
		};
		this.current = {}

		angular.copy(this.default, this.current)

	}

	updateSchedule(){
		if(!this.current.frequency){
			this.current.schedule = []
		}else{
			this.current.schedule = this.service.getSchedule(this.current.frequency,5,this.current.start,this.current.end);
		}
	}

	interpolateDescription(template, date){
		var f = this.dateFilter;
		return template
			.replace('[month]', f(date,'MM') )
			.replace('[day]', f(date,'d') )
			.replace('[year]', f(date,'yy') )
	}

	setFrequency(frequency,start, end){
		this.current.start = start || new Date();
		this.current.end = end || undefined;
		this.current.frequency = frequency || []
		return this.updateSchedule();
	}
}