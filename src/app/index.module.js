/* global moment:false */

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { MainController } from './main/main.controller';
import { Invoices } from './invoices/invoices.module'
import { NavbarDirective } from '../app/components/navbar/navbar.directive';
import { Components } from '../app/components/components.module';
import later from 'later';

angular.module('faktury', [
	'ngAnimate',
	'ngMessages',
	'ui.router',
	'ui.bootstrap',
	'invoices',
	'components'])
  .value('moment', moment)
  .value('later', later)
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .controller('MainController', MainController)
  .directive('navbar', NavbarDirective);
