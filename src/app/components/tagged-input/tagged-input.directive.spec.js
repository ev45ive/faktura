describe('tagged-input directive', function(){
	let element
		,input
		,scope
		//,isolateScope;

	beforeEach(angular.mock.module('faktury'));

	beforeEach(inject(($compile, $rootScope) => {
		element = angular.element(`
			<tagged-input ng-model="test_value" tags="tags"></tagged-input>
		`);
		scope = $rootScope.$new();

		scope.test_value = 'Test content';
		scope.tags = {
			tag1: '[tag1]',
			tag2: '[tag2]'
		}

		$compile(element)(scope);
		$rootScope.$digest();

		input = element.find('[ng-model]');
		//isolateScope = element.isolateScope();
	}));

	it('should be compiled', () => {
		expect(element.html()).not.toEqual('');
	})
	
	it('should render input', () => {
		expect(input.val()).toMatch(scope.test_value);
	})
	
	it('should update input to model value', () => {
		scope.test_value = 'New value';
		scope.$digest();

		expect(input.val()).toEqual(scope.test_value);
	})
})