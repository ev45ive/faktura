import { TaggedInputDirective } from './tagged-input.directive';

angular.module('components.tagged-input',[])
.directive('taggedInput', TaggedInputDirective)

