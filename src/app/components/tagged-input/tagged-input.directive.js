export function TaggedInputDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    //replace: true,
    templateUrl: 'app/components/tagged-input/tagged-input.html',
    controller: TaggedInputController,
    controllerAs: 'tagged',
    bindToController: true,
    scope:{
      value: '=ngModel'
    }/*,
    link: function(scope, $element, attrs, controller, transclude){

    }*/
  };

  return directive;
}

class TaggedInputController{
  constructor () {
    'ngInject';
  }
}
