export function TaggedTextDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/tagged-input/tagged-text.html',
    controller: PanelController,
    controllerAs: 'tagged',
    bindToController: true,
    link: function(scope, $element, attrs, controller, transclude){

    }
  };

  return directive;
}

class TaggedTextController{
  constructor () {
    'ngInject';
  }
}
