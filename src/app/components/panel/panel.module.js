
import { PanelDirective } from './panel.directive';

angular.module('components.panel',[])
  .directive('panel',PanelDirective)