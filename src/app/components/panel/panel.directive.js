export function PanelDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'app/components/panel/panel.html',
    transclude:{
      'head':'?panelHead',
      'body':'?panelBody',
      'footer':'?panelFooter'
    },
    controller: PanelController,
    controllerAs: 'panel',
    bindToController: true,
    link: function(scope, $element, attrs, controller, transclude){
      controller.head = transclude.isSlotFilled('head');
      controller.body = transclude.isSlotFilled('body');
      controller.footer = transclude.isSlotFilled('footer');
    }
  };

  return directive;
}

class PanelController{
  constructor () {
    'ngInject';
  }
}
