describe('panel directive', function(){
	let element
		,scope
		//,isolateScope;

	beforeEach(angular.mock.module('faktury'));

	beforeEach(inject(function($compile, $rootScope){
		element = angular.element(`
			<panel>
				<panel-head>
					header
				</panel-head>
				<panel-body>
					body
				</panel-body>
				<panel-footer>
					footer
				</panel-footer>
			</panel>
		`);
		scope = $rootScope.$new();
		$compile(element)(scope);
		$rootScope.$digest();
		//isolateScope = element.isolateScope();
	}));

	it('should be compiled', function(){
		expect(element.html()).not.toEqual('');
	})
	
	it('should transclude body', function(){
		expect(element.find('.panel-body').html()).toMatch('body');
	})
	
	it('should transclude header', function(){
		expect(element.find('.panel-heading').html()).toMatch('header');
	})
	
	it('should transclude footer', function(){
		expect(element.find('.panel-footer').html()).toMatch('footer');
	})
})