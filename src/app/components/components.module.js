import { Panel } from './panel/panel.module';
import { TaggedInput } from './tagged-input/tagged-input.module';

angular.module('components',[
	'components.panel',
	'components.tagged-input'
])