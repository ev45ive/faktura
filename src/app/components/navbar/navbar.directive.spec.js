/**
 * @todo Complete the test
 * This example is not perfect.
 * Test should check if MomentJS have been called
 */
describe('directive navbar', function() {
  let element;

  beforeEach(angular.mock.module('faktury'));

  beforeEach(inject(($compile, $rootScope) => {

    element = angular.element(`
      <navbar></navbar>
    `);

    $compile(element)($rootScope.$new());
    $rootScope.$digest();
  }));

  it('should be compiled', () => {
    expect(element.html()).not.toEqual(null);
  });
});
